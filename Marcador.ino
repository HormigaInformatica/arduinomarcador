#include "Timer.h"
#include <SPI.h>
#include <Ethernet.h>
#include <SD.h>

#define REQ_BUF_SZ   20

const bool debug = true;
const int MinutosCero =7;
const int MinutosUno =8;
const int SegundosCero=9;
const int SegundosUno=10;
const int Local0 =2;
const int Local1 =3;
const int Visit0 =5;
const int Visit1= 6;

Timer t;
char rootFileName[] = "marca.htm";
char clientline[BUFSIZ];
char *filename;
int index = 0;
int image = 0;

byte mac[] = { 0x90, 0xA2, 0xDA, 0x0D, 0xE0, 0x43 };
IPAddress ip(192, 168,0, 200); // IP address, may need to change depending on network
EthernetServer server(80);  // create a server at port 80
char HTTP_req[REQ_BUF_SZ] = {0}; // buffered HTTP request stored as null terminated string
char req_index = 0;   
const int Segmentos[7] = {A2,A1,A3,A0,13,A4,A5};

int hora_hora=0;
int hora_min=0;
int hora_seg=0;

const int Numeros[11][7]={{1,1,1,0,1,1,1},{0,0,1,0,0,1,0},{1,0,1,1,1,0,1},{1,0,1,1,0,1,1},{0,1,1,1,0,1,0},{1,1,0,1,0,1,1},{1,1,0,1,1,1,1},{1,0,1,0,0,1,0},{1,1,1,1,1,1,1},{1,1,1,1,0,1,0},{0,0,0,0,0,0,0}};

File webFile;

int Hasta = 0;
bool paused=1;
unsigned long segundo = 0;
int estado = 0;
String enviado="";
boolean currentLineIsBlank = true;
char c = ' ';
int Minutos=0;
int Segundos=0;
int Local = 0;
int Visitante = 0;

void escribeuno(int num,int cual){
   // if (debug)Serial.print("Numero: "+String(num)+" -> ");
    for (int i=0; i<7; i++)
      {
        digitalWrite(Segmentos[i], Numeros[num][i]);
        //if (debug)Serial.print(String(Segmentos[i])+"|"+String(Numeros[num][i])+"//");
      }
   // if (debug)Serial.print(" -> "+String(cual));
   // if (debug)Serial.println("");
      
    digitalWrite(cual, HIGH);
    delay(20);
    digitalWrite(cual, LOW);
    delay(5);
}

void setup() {
 if (debug) Serial.begin(9600);

//  if (debug) Serial.println(FreeRam());

  pinMode(10, OUTPUT); // set the SS pin as an output (necessary!)
  digitalWrite(10, HIGH); // but turn off the W5100 chip!

  if (!SD.begin(4)) {
//    Serial.println("ERROR - SD card initialization failed!");
    return; // init failed
  }

  Ethernet.begin(mac, ip); // initialize Ethernet device
  server.begin();

  pinMode(Segmentos[0], OUTPUT);
  pinMode(Segmentos[1], OUTPUT);
  pinMode(Segmentos[2], OUTPUT);
  pinMode(Segmentos[3], OUTPUT);
  pinMode(Segmentos[4], OUTPUT);
  pinMode(Segmentos[5], OUTPUT);
  pinMode(Segmentos[6], OUTPUT);
  pinMode(MinutosCero, OUTPUT);
  pinMode(SegundosCero, OUTPUT);
  pinMode(Local0, OUTPUT);
  pinMode(Visit0, OUTPUT);
  pinMode(MinutosUno, OUTPUT);
  pinMode(SegundosUno, OUTPUT);
  pinMode(Local1, OUTPUT);
  pinMode(Visit1, OUTPUT);
  escribeuno(0, Local0);
  escribeuno(0, Local1);
  escribeuno(0, Visit0);
  escribeuno(0, Visit1);
  escribeuno(0, MinutosCero);
  escribeuno(0, MinutosUno);
  escribeuno(0, SegundosCero);
  escribeuno(0, SegundosUno);
  t.every(1000, funciona);
}

void loop() {
  t.update();

  EthernetClient client = server.available(); // try to get client
  if (client) {
    boolean current_line_is_blank = true;
    index = 0;

    while (client.connected()) {
      if (client.available()) {
        char c = client.read();

        if (c != '\n' && c != '\r') {
          clientline[index] = c;
          index++;
          if (index >= BUFSIZ)
            index = BUFSIZ - 1;

          // continue to read more data!
          continue;
        }

        clientline[index] = 0;
        filename = 0;

        //if (debug)Serial.println(clientline);

        if (strstr(clientline, "GET / ") != 0) {
          filename = rootFileName;
          webFile = SD.open(rootFileName);
        } else if (strstr(clientline, "recibir") != 0) {
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Cache-Control: max-age=-1");
          client.println("Cache-Control: no-cache");
          client.println("Connnection: close");
          client.println();
          String enviarminutos = String(Minutos);
          String enviarsegundos = String(Segundos);
          String enviarlocal = String(Local);
          String enviarvisitante = String(Visitante);
          if (Minutos < 10) enviarminutos = "0" + enviarminutos;
          if (Segundos < 10) enviarsegundos = "0" + enviarsegundos;
          if (Local < 10) enviarlocal = "0" + enviarlocal;
          if (Visitante < 10) enviarvisitante = "0" + enviarvisitante;

          client.println(enviarminutos + "//" + enviarsegundos + "//" + enviarlocal + "//" + enviarvisitante + "//" + String(Hasta) + "//" + String(paused) + "//");
          //if (debug)Serial.println(enviarminutos+"//"+enviarsegundos+"//"+String(Local)+"//"+String(Visitante)+"//"+String(Hasta)+"//"+String(paused)+"//");
          delay(2);
          client.stop();
          break;
        } else if (strstr(clientline, "comandos") != 0) {
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connnection: close");
          client.println();
          enviado = clientline + 5;
          client.println("OK");
          delay(2);
          client.stop();
          if (enviado != "") {
 //           if (debug) Serial.println("Recibidos valores");
 //           if (debug) Serial.println(enviado);
            Local = getVal(enviado, ',', 1).toInt();
//            if (debug) Serial.println("Local:" + String(Local));
            Visitante = getVal(enviado, ',', 2).toInt();
//            if (debug) Serial.println("Visitante:" + String(Visitante));

            if (Hasta != getVal(enviado, ',', 3).toInt()) {
              Hasta = getVal(enviado, ',', 3).toInt();
  //            if (debug) Serial.println("Hasta:" + String(Hasta));
              paused = 1;
              Segundos = 0;
              if (Hasta == 70) Minutos = 35;
              else if (Hasta == 80) Minutos = 40;
              else if (Hasta == 90) Minutos = 45;

              escribetiempo(Minutos, Segundos);
            }
            estado = getVal(enviado, ',', 4).toInt();
//            if (debug) Serial.println("estado:" + String(estado));
            if (estado == 0) paused = 1;
            else if (estado == 1) paused = 0;
            else if (estado == 2) {
              paused = 1;
              Local = 0;
              Visitante = 0;
              Segundos = 0;
              Minutos = 0;
              escribetiempo(0, 0);
            } else if (estado == 5) {
              hora_hora = Local;
              hora_min = Visitante;
              hora_seg = 0;

              Local = 0;
              Visitante = 0;
              Minutos = 0;
              Segundos = 0;
              estado = 6;
            }
            enviado = "";
          }
          break;
        }
        if (strstr(clientline, "GET /") != 0) {

          if (!filename) filename = clientline + 5;
          (strstr(clientline, " HTTP"))[0] = 0;

//          if (debug) Serial.println(filename);
          if (!SD.exists(filename)) {
//            if (debug) Serial.println("No existe ");
            client.println("HTTP/1.1 404 Not Found");
            client.println("Content-Type: text/html");
            client.println();
            client.println("<h2>File Not Found!</h2>");
            break;
          }

//          if (debug) Serial.println("Opened!");
          webFile = SD.open(filename);
          client.println("HTTP/1.1 200 OK");
          client.println("Cache-Control: max-age=99592000");
          client.println("Cache-Control: public");
          if (strstr(filename, ".htm") != 0)
            client.println("Content-Type: text/html");
          else if (strstr(filename, ".css") != 0)
            client.println("Content-Type: text/css");
          else if (strstr(filename, ".png") != 0)
            client.println("Content-Type: image/png");
          else if (strstr(filename, ".jpg") != 0)
            client.println("Content-Type: image/jpeg");
          else if (strstr(filename, ".gif") != 0)
            client.println("Content-Type: image/gif");
          else if (strstr(filename, ".3gp") != 0)
            client.println("Content-Type: video/mpeg");
          else if (strstr(filename, ".pdf") != 0)
            client.println("Content-Type: application/pdf");
          else if (strstr(filename, ".js") != 0) {
            client.println("Content-Type: application/x-javascript");
            client.println("Content-Encoding: gzip");
          } else if (strstr(filename, ".xml") != 0)
            client.println("Content-Type: application/xml");
          else
            client.println("Content-Type: text");

          client.println();

          if (webFile) {
            byte buf[1024];
            while (1) {
              int n = webFile.available();
              if (n == 0) break;
              if (n > 1024) n = 1024;
              webFile.read(buf, n);
              client.write(buf, n);
            }
            webFile.close();
          }

        } else {
          client.println("HTTP/1.1 404 Not Found");
          client.println("Content-Type: text/html");
          client.println();
          client.println("<h2>File Not Found!</h2>");
        }
        break;
      }
    }
    delay(1);
    client.stop();
  }

}

void funciona() {


  if (estado == 6) {
    hora_seg = hora_seg + 1;
    if (hora_seg == 60) {
      hora_seg = 0;
      hora_min = hora_min + 1;
    }
    if (hora_min == 60) {
      hora_min = 0;
      hora_hora = hora_hora + 1;
    }
    if (hora_hora > 12) {
      hora_hora = hora_hora - 12;
    }
    Minutos = hora_hora;
    Segundos = hora_min;
    escribetiempo(hora_hora, hora_min);
  } else if (paused == 0) {

    if (Minutos >= Hasta) {
      paused = 1;
    } else if (Segundos == 0 and Minutos == Hasta) {
//     if (debug) Serial.println("pausar por llegada a limite");
      paused = 1;
    } else if (Segundos == 59 and Minutos < Hasta) {
//      if (debug) Serial.println("Debe subir un minuto");
      Minutos = Minutos + 1;
      Segundos = 0;
      escribetiempo(Minutos, Segundos);
    } else if (Segundos < 60) {
      // if (debug)Serial.println("Segundos: "+String(Segundos)+", Minutos: "+Minutos+" - ANTES");
      Segundos = Segundos + 1;

      escribetiempo(Minutos, Segundos);
      //if (debug)Serial.println("Segundos: "+String(Segundos)+", Minutos: "+Minutos+" - Despues");
    }
  }
  escribeuno(String(Local).substring(0, 1).toInt(), Local0);
  escribeuno(String(Local).substring(1).toInt(), Local1);
  escribeuno(String(Visitante).substring(0, 1).toInt(), Visit0);
  escribeuno(String(Visitante).substring(1).toInt(), Visit1);
 Serial.println("Local:" + String(Local) + ", Visitante:" + String(Visitante) + ", Minutos:" + String(Minutos) + ", Segundos:" + String(Segundos) + ",Hasta:" + String(Hasta) + ",Paused:" + String(paused)+", Estado: "+estado);
}

void escribetiempo(int Mins, int Segs) {

  String vMins = String(Mins);
  String vSegs = String(Segs);
  if (Mins < 10)
    vMins = "0" + String(Mins);
  if (Segs < 10)
    vSegs = "0" + String(Segs);
  //if(debug)Serial.println("Mins: "+String(Mins)+", Segs: "+String(Segs)+", vMins: "+vMins+", vSegs: "+vSegs);

  escribeuno(vMins.substring(0, 1).toInt(), MinutosCero);
  escribeuno(vMins.substring(1).toInt(), MinutosUno);
  escribeuno(vSegs.substring(0, 1).toInt(), SegundosCero);
  escribeuno(vSegs.substring(1).toInt(), SegundosUno);
}

String getVal(String data, char separator, int index) {
  int found = 0;
  int strIndex[] = {
    0,
    -1
  };
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

char StrContains(char * str, char * sfind) {
  char found = 0;
  char index = 0;
  char len;

  len = strlen(str);

  if (strlen(sfind) > len) {
    return 0;
  }
  while (index < len) {
    if (str[index] == sfind[found]) {
      found++;
      if (strlen(sfind) == found) {
        return 1;
      }
    } else {
      found = 0;
    }
    index++;
  }

  return 0;
}
void StrClear(char * str, char length) {
  for (int i = 0; i < length; i++) {
    str[i] = 0;
  }
}